# About

The project contains a C++ implementation of ray tracing and path tracing algorithms. 

Features:

* ray tracing / path tracing implemented separately
* rendering complex 3D models, loaded from .FBX files
* volumetric path tracing 
* textures
* depth of field 
* SSAA - supersampling antialiasing 
* stratified sampling

# Dependencies 

DirectX SDK needs to be installed: https://www.microsoft.com/en-us/download/details.aspx?id=6812

# Usage

The main.cpp file contains the code for rendering a Cornell box scene. 

For volumetric path tracing, simply modify the `scattering` component of the environment material passed in `pathtracer`'s constructor. 

Rendering textures can be a bit tricky; I'll describe the procedure I use:

1. Open downloaded (or create your own) 3D model in 3DS Max
2. Assign texture by creating a new Phong material with bitmap as diffuse component
3. Export as .fbx - when exporting, exclude lights and camera, in the "Geometry" tab only include "Tangents and Binormals" and "Triangulate"
4. Instantiate texture with the image you used, assign to material:

```c++
texture my_tex("path-to-image");
material tex_material(color(1, 1, 1), 0, 0, 0, 0, 1, 0, 0, std::make_shared<texture>(my_tex));

```
and assign material to `mesh` object (don't forget to set its `has_texture` property to `true`).

