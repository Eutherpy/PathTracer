#pragma once
#include <cmath>
#include "vector3.h"

namespace util
{
	namespace random
	{
		raycasting::vector3 randCone(double, const raycasting::vector3&);
		raycasting::vector3 randHemisphere(double, double, const raycasting::vector3&);
		double randomNumUniform(double, double);
		double randNumExp(double exp);
		raycasting::vector3 randSphere();
	}
}

