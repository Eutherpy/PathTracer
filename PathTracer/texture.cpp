#include <math.h>
#include <windows.h>
#include <objidl.h>
#include <gdiplus.h>
#include "texture.h"
#include "util.h"

using namespace Gdiplus;

#pragma comment (lib,"Gdiplus.lib")

namespace raycasting
{
	raycasting::texture::texture(const char *filename)
	{
		//start GDI+
		Gdiplus::GdiplusStartupInput gdiplusStartupInput;
		ULONG_PTR gdiplusToken;
		Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

		//load image
		Bitmap *bmp = new Bitmap(util::toWcharPtr(filename), false);
		int width = bmp->GetWidth(), height = bmp->GetHeight();
		img = image(width, height);
		for (int i = 0; i < width; ++i)
			for (int j = 0; j < height; ++j)
			{
				Gdiplus::Color col;
				bmp->GetPixel(i, j, &col);
				img.img[i][j] = color(col.GetR() / 255.0, col.GetG() / 255.0, col.GetB() / 255.0);
			}

		//shut down GDI+
		delete bmp;
		Gdiplus::GdiplusShutdown(gdiplusToken);
	}

	color texture::bilinearIntp(double x, double y)
	{
		//find 4 nearest pixels
		int x1 = static_cast<int>(floor(x)), x2 = static_cast<int>(ceil(x));
		int y1 = static_cast<int>(floor(y)), y2 = static_cast<int>(ceil(y));
		color c1 = img.img[x1][y1];
		color c2 = img.img[x2][y1];
		color c3 = img.img[x1][y2];
		color c4 = img.img[x2][y2];
		color  a = c1 + (x - x1)*(c2 - c1);
		color  b = c3 + (x - x1)*(c4 - c3);
		return a + (y - y1)*(b - a);
	}

	color texture::operator[](const uv_coords &uv)
	{
		return bilinearIntp(uv.u * (img.width - 1), uv.v * (img.height - 1));
	}
}
