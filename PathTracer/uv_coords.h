#pragma once

namespace raycasting
{
	struct uv_coords
	{
		double u, v;
		uv_coords(double = -1, double = -1);
		uv_coords operator*(double) const;
		friend uv_coords operator*(double, const uv_coords&);
		uv_coords operator+(const uv_coords&) const;
	};
}
