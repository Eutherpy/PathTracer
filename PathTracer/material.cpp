#include "material.h"

using std::shared_ptr;

namespace raycasting
{
	raycasting::material::material(const color &color, double transparency, double reflectivity, double refraction_index, double emissivity, double diffuseness, double scattering, double smoothness, shared_ptr<texture> tex) :
		col(color), transparency(transparency), reflectivity(reflectivity), refraction_index(refraction_index), emissivity(emissivity), diffuseness(diffuseness), scattering(scattering), smoothness(smoothness), tex(tex) { }
}