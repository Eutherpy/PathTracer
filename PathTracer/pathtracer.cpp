#include "pathtracer.h"
#include "util.h"

using std::initializer_list;
using std::get;

using namespace util::random;

namespace raycasting
{
	const int STRATIFICATION_DEPTH = 2;

	pathtracer::pathtracer(const camera &cam, const initializer_list<const shape*> &shapes, const material &curr_mat,
		std::shared_ptr<realtime_drawing_manager> drm) : renderer(cam, shapes, curr_mat, drm) { }

	color pathtracer::radiance(int current_depth, int depth, const ray &curr_ray, const material &surrounding_mat, int diffuse_depth) const
	{
		if (current_depth == depth)
			return color(0, 0, 0);

		//scatter current ray based on surrounding material
		double rand_point = 0;
		ray scattered_ray;
		if (environment_mat.scattering > 0)
		{
			rand_point = randNumExp(environment_mat.scattering);
			scattered_ray = ray::ray(curr_ray.origin + curr_ray.direction*rand_point, randSphere());
		}

		//intersect with objects
		auto res = util::intersectObjects(shapes, curr_ray, false, false);
		auto intrs = get<0>(res);
		if (!intrs.valid) //no intersections
			return (environment_mat.scattering > 0) ? environment_mat.col * radiance(current_depth + 1, depth, scattered_ray, environment_mat, diffuse_depth) : color(0, 0, 0);
		//scatter current ray
		double min_distance = get<1>(res);
		if (environment_mat.scattering > 0 && rand_point*rand_point < min_distance)
			return radiance(current_depth + 1, depth, scattered_ray, environment_mat, diffuse_depth);

		if (intrs.mat.emissivity > 0)
			return intrs.mat.col * intrs.mat.emissivity;

		bool is_diffuse = intrs.mat.diffuseness > 0;
		diffuse_depth += is_diffuse;

		color diffuse(0, 0, 0);
		if (intrs.mat.smoothness > 0)
		{
			if (diffuse_depth < STRATIFICATION_DEPTH && is_diffuse)
			{
				for (int i = 0; i < 4; ++i)
				{
					ray new_ray(intrs.point, (randCone((1 - intrs.mat.smoothness)*M_PI, intrs.normal).normalize()));
					diffuse += radiance(current_depth + 1, depth, new_ray, environment_mat, diffuse_depth);
				}
				diffuse *= (intrs.mat.diffuseness * intrs.mat.col)*0.25;
			}
			else
			{
				ray new_ray(intrs.point, (randCone((1 - intrs.mat.smoothness)*M_PI, intrs.normal).normalize()));
				diffuse = intrs.mat.diffuseness * intrs.mat.col * radiance(current_depth + 1, depth, new_ray, environment_mat, diffuse_depth);
			}
		}
		else
			//generate a random direction - stratified sampling	
			if (diffuse_depth < STRATIFICATION_DEPTH && is_diffuse)
			{
				ray new_ray1(intrs.point, (randHemisphere(randomNumUniform(0, 0.5), randomNumUniform(0, 0.5), intrs.normal)).normalize());
				ray new_ray2(intrs.point, (randHemisphere(randomNumUniform(0, 0.5), randomNumUniform(0.5, 1), intrs.normal)).normalize());
				ray new_ray3(intrs.point, (randHemisphere(randomNumUniform(0.5, 1), randomNumUniform(0, 0.5), intrs.normal)).normalize());
				ray new_ray4(intrs.point, (randHemisphere(randomNumUniform(0.5, 1), randomNumUniform(0.5, 1), intrs.normal)).normalize());
				diffuse = (radiance(current_depth + 1, depth, new_ray1, environment_mat, diffuse_depth) + radiance(current_depth + 1, depth, new_ray2, environment_mat, diffuse_depth) + radiance(current_depth + 1, depth, new_ray3, environment_mat, diffuse_depth) + radiance(current_depth + 1, depth, new_ray4, environment_mat, diffuse_depth))*0.25;
				diffuse *= (intrs.mat.diffuseness * intrs.mat.col);
			}
			else
			{
				ray new_ray(intrs.point, (randHemisphere(randomNumUniform(0, 1), randomNumUniform(0, 1), intrs.normal)).normalize());
				diffuse = intrs.mat.diffuseness * intrs.mat.col * radiance(current_depth + 1, depth, new_ray, environment_mat, diffuse_depth);
			}
		//reflection
		color reflected(0, 0, 0);
		if (intrs.mat.reflectivity > 0)
			reflected = intrs.mat.reflectivity * intrs.mat.col * radiance(current_depth + 1, depth, ray::ray(intrs.point, ((-curr_ray.direction).reflect(intrs.normal)).normalize()), environment_mat, diffuse_depth);
		//refraction
		color refracted(0, 0, 0);
		if (intrs.mat.transparency > 0)
		{
			vector3 b = (-curr_ray.direction) % intrs.normal;
			double sin_theta_one = b.magnitude();
			double n1 = surrounding_mat.refraction_index, n2 = intrs.mat.refraction_index;
			double sin_theta_two = n1 / n2 * sin_theta_one;
			//total internal reflection
			if (sin_theta_two > 1)
				refracted = radiance(current_depth + 1, depth, ray::ray(intrs.point, ((-curr_ray.direction).reflect(intrs.normal)).normalize()), environment_mat, diffuse_depth);
			else
				refracted = radiance(current_depth + 1, depth, ray::ray(intrs.point, ((-intrs.normal).rotate(b, -asin(sin_theta_two))).normalize()), intrs.inside ? surrounding_mat : intrs.mat, diffuse_depth);
			refracted *= (intrs.mat.transparency * intrs.mat.col);
		}
		return (diffuse + reflected + refracted);
	}
}
