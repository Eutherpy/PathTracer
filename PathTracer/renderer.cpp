#include <iostream>
#include <ctime>
#include "renderer.h"
#include "util.h"

using std::vector;
using std::initializer_list;
using std::get;

using namespace util::random;

namespace raycasting
{
	raycasting::renderer::renderer(const camera &cam, const initializer_list<const shape*> &shapes, const material &curr_mat,
		std::shared_ptr<realtime_drawing_manager> drm) : cam(cam), environment_mat(curr_mat), drawing_manager(drm)
	{
		for (auto &x : shapes)
			this->shapes.push_back(x);
	}

	void renderer::render(int num_iterations, int depth, const char* filename)
	{
		drawing_manager->start();
		std::thread rendering_thread(&renderer::iterate, this, num_iterations, depth, filename);
		drawing_manager->messageloop();
		rendering_thread.join();
		drawing_manager->stop();
	}

	void raycasting::renderer::iterate(int num_iterations, int depth, const char* filename)
	{
		clock_t begin = clock();
		//drawing_manager->start();
		for (int i = 0; i < num_iterations; ++i)
		{
			double percentage = 100.0 * i / num_iterations;
			trace(num_iterations, depth, environment_mat);
			clock_t end = clock();
			int time_left = static_cast<int>((end - begin) / CLOCKS_PER_SEC / percentage * (100 - percentage));
			int mins = time_left / 60, hrs = mins / 60;
			if (i != 0)
				std::cout << percentage << "% done. ETA: " << hrs << ":" << mins % 60 << ":" << time_left % 60 << std::endl;
			drawing_manager->draw(cam.img.img, i);
		}
		cam.img.write(filename, num_iterations);
		drawing_manager->stop();
	}

	void renderer::trace(int num_iterations, int depth, const material &surrounding_mat)
	{
#pragma omp parallel for schedule(dynamic)
		for (int i = 0; i < cam.cam_width; ++i)
			for (int j = 0; j < cam.cam_height; ++j)
			{
				ray pixel_ray;
				if (cam.dof > 0)
				{
					vector3 rand_vec = cam.up.rotate(cam.direction, randomNumUniform(-M_PI, M_PI)), new_origin = cam.origin + rand_vec * randomNumUniform(0, 1)*cam.dof;
					pixel_ray = ray(new_origin, (cam.origin + cam.screen[i][j] - new_origin).normalize());
				}
				else
					pixel_ray = ray(cam.origin, (cam.screen[i][j] - cam.origin).normalize());
				color path_col = radiance(1, depth, pixel_ray, surrounding_mat, 0);
				cam.img.img[i / cam.ssaa_coeff][j / cam.ssaa_coeff] += path_col * (1.0 / (cam.ssaa_coeff*cam.ssaa_coeff)); //SSAA
			}
	}
}