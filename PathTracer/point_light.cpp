#include "point_light.h"

namespace raycasting
{
	raycasting::point_light::point_light(const vector3 &p, const color &col, double intensity) :
		point(p), light_color(col), light_intensity(intensity) { }
}

