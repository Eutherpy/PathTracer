#pragma once
#include "vector3.h"

namespace raycasting
{
	struct color
	{
		double R, G, B;
		color() = default;
		color(double, double, double);
		color operator+(const color&) const;
		color operator-(const color&) const;
		color& operator+=(const color&);
		color& operator-=(const color&);
		color operator*(double) const;
		friend color operator*(double, const color&);
		color operator*(const color&) const;
		color& operator*=(double);
		color& operator*=(const color&);
	};
}
