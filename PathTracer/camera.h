#pragma once
#include <vector>
#include "vector3.h"
#include "image.h"

namespace raycasting
{
	struct camera
	{
		vector3 origin, direction, up;
		double angle;
		int width, height;
		int cam_width, cam_height;
		int ssaa_coeff;
		std::vector<std::vector<vector3>> screen;
		image img;
		double dof;
		camera() = default;
		camera(const vector3&, const vector3&, double, int, int, double, int);
	};
}
