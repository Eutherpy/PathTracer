#pragma once
#include <vector>
#include <memory>
#include <thread>
#include "vector3.h"
#include "camera.h"
#include "shape.h"
#include "random.h"
#include "realtime_drawing_manager.h"
#include "directx_manager.h"

namespace raycasting
{
	class renderer
	{
	protected:
		std::vector<const shape*> shapes;
		camera cam;
		material environment_mat;
		std::shared_ptr<realtime_drawing_manager> drawing_manager;
	public:
		renderer() = default;
		renderer(const camera&, const std::initializer_list<const shape*>&, const material&, std::shared_ptr<realtime_drawing_manager>);
		void render(int, int, const char*);
		void iterate(int, int, const char*);
		void trace(int, int, const material&);
		virtual color radiance(int, int, const ray&, const material&, int) const = 0;
	};
}
