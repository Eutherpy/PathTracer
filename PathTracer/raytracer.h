#pragma once
#include <vector>
#include <memory>
#include <thread>
#include "vector3.h"
#include "camera.h"
#include "shape.h"
#include "random.h"
#include "realtime_drawing_manager.h"
#include "directx_manager.h"
#include "renderer.h"
#include "point_light.h"

namespace raycasting
{
	class raytracer : public virtual renderer
	{
	protected:
		std::vector<point_light*> lights;
		double shadow_darkness;
	public:
		raytracer() = default;
		raytracer(const camera&, const std::initializer_list<const shape*>&, const material&, std::shared_ptr<realtime_drawing_manager>, const std::vector<point_light*>&, double);
		virtual color radiance(int, int, const ray&, const material&, int) const override;
	};
}
