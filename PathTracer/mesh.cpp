#include <algorithm> 
#include <tuple> 
#include <iterator> 
#include "mesh.h"
#include "shape.h"
#include "util.h"
#include "uv_coords.h"

using std::initializer_list;
using std::vector;
using std::get;

using namespace util;

namespace raycasting
{
	mesh::mesh(const initializer_list<triangle> &triangles, const initializer_list<const transform*> &transforms)
	{
		/*scale, rotate and translate into global coordinates*/
		this->triangles = transformTriangles(triangles, transforms);

		/*calculate bounding sphere parameters*/
		this->bounding_sphere = calculateBoundingSphere();
	}

	mesh::mesh(const char *filename, const initializer_list<const transform*> &transforms, const material &mat, bool interpolate_normals, bool has_texture)
	{
		this->interpolate_normals = interpolate_normals;
		this->mat = mat;

		const char *fbx_filename = filename;
		FbxManager *fbx_manager = FbxManager::Create();
		FbxIOSettings* fbx_io_settings = FbxIOSettings::Create(fbx_manager, IOSROOT);
		fbx_manager->SetIOSettings(fbx_io_settings);
		FbxImporter *fbx_importer = FbxImporter::Create(fbx_manager, "");
		if (!fbx_importer->Initialize(fbx_filename, -1, fbx_io_settings))
			throw std::exception("Failed to initialize FBX importer.");
		FbxScene* scene = FbxScene::Create(fbx_manager, "scene");
		fbx_importer->Import(scene);
		fbx_importer->Destroy();
		FbxMesh *mesh = FbxMesh::Create(scene, "");
		FbxNode *node = scene->GetRootNode()->GetChild(0);
		FbxSurfaceMaterial *fbx_mat = node->GetMaterial(0);

		color col;

		//handle only Phong materials
		if (fbx_mat != nullptr && fbx_mat->GetClassId().Is(FbxSurfacePhong::ClassId))
		{
			FbxDouble3 mat_col = ((FbxSurfacePhong *)fbx_mat)->Diffuse;
			col = color(mat_col[0], mat_col[0], mat_col[0]);
		}
		else
			col = mat.col;

		FbxGeometryConverter fbx_converter(node->GetFbxManager());
		fbx_converter.Triangulate(node->GetNodeAttribute(), true);
		mesh = node->GetMesh();
		int num_vertices = mesh->GetPolygonVertexCount();
		FbxVector4 *fbx_vertices = mesh->GetControlPoints();
		int *fbx_triangle_vertices = mesh->GetPolygonVertices();
		double x, y, z;
		vector<vertex> vertices;
		FbxGeometryElementNormal *fbx_normal_elem = mesh->GetElementNormal();
		auto normals = fbx_normal_elem->GetDirectArray();
		FbxGeometryElementUV *fbx_uv_elem;
		if (!has_texture)
		{
			for (int vertex_index = 0; vertex_index < num_vertices; ++vertex_index)
			{
				int index = fbx_triangle_vertices[vertex_index];
				x = (double)fbx_vertices[index][0];
				y = (double)fbx_vertices[index][1];
				z = (double)fbx_vertices[index][2];
				FbxVector4 fbx_normal = normals.GetAt(vertex_index);
				vector3 normal = vector3(fbx_normal[0], fbx_normal[1], fbx_normal[2]);
				vertices.push_back(vertex(vector3(x, y, z), vertex_data(normal)));
			}
		}
		else
		{
			fbx_uv_elem = mesh->GetElementUV();
			auto uv_coordinates = fbx_uv_elem->GetDirectArray();
			for (int vertex_index = 0; vertex_index < num_vertices; ++vertex_index)
			{
				int index = fbx_triangle_vertices[vertex_index];
				x = (double)fbx_vertices[index][0];
				y = (double)fbx_vertices[index][1];
				z = (double)fbx_vertices[index][2];
				FbxVector4 fbx_normal = normals.GetAt(vertex_index);
				vector3 normal = vector3(fbx_normal[0], fbx_normal[1], fbx_normal[2]);
				FbxVector2 fbx_uv = uv_coordinates.GetAt(fbx_uv_elem->GetIndexArray().GetAt(vertex_index));
				uv_coords uv = uv_coords(fbx_uv[0], fbx_uv[1]);
				vertices.push_back(vertex(vector3(x, y, z), vertex_data(normal, uv)));
			}
		}

		vector<triangle> temp_triangles;
		for (int i = 0; i < num_vertices; i += 3)
			temp_triangles.push_back(triangle(vertices[i], vertices[i + 1], vertices[i + 2],
				material(mat.col, mat.transparency, mat.reflectivity, mat.refraction_index, mat.emissivity, mat.diffuseness, mat.scattering, mat.smoothness, nullptr)));
		this->triangles = transformTriangles(temp_triangles, transforms);
		this->bounding_sphere = calculateBoundingSphere();
	}

	vector<triangle> mesh::transformTriangles(const std::vector<triangle> &triangles, const initializer_list<const transform*> &transforms) const
	{
		vector<triangle> new_triangles;
		/*transform triangles*/
		for (auto &t : triangles)
			new_triangles.push_back(transform::applyTransforms(t, transforms));
		return new_triangles;
	}

	sphere mesh::calculateBoundingSphere() const
	{
		/*calculate bounding sphere parameters*/

		vector<vector3> points = getAllPoints();
		//calculate leftmost and rightmost point of the mesh
		decltype(points)::iterator leftmost, rightmost;
		std::tie(leftmost, rightmost) = std::minmax_element(begin(points), end(points),
			[](vector3 const& p1, vector3 const& p2)
		{
			return p1.getZ() > p2.getZ();
		});
		vector3 lm = *leftmost, rm = *rightmost;
		double t = lm.distance(rm) / 2;
		vector3 center = lm + t*(rm - lm).normalize();
		//calculate furthest point from center
		decltype(points)::iterator furthest;
		furthest = std::max_element(begin(points), end(points),
			[&center](vector3 const& p1, vector3 const& p2)
		{
			return p1.distance(center) < p2.distance(center);
		});
		double radius = furthest->distance(center);
		return sphere(center, radius, material());
	}

	shape::intersection raycasting::mesh::intersect(const ray &curr_ray, bool interpolate_normals = false) const
	{
		auto bs_intrs = bounding_sphere.intersect(curr_ray, false);
		if (!bs_intrs.valid)
			return intersection();
		vector<const shape*> triangle_ptrs;
		for (auto &x : triangles)
			triangle_ptrs.push_back(&x);
		auto intrs_temp = get<0>(intersectObjects(triangle_ptrs, curr_ray, true, interpolate_normals));
		material tm = intrs_temp.mat;
		material mat;
		if (intrs_temp.uv.u < 0 && intrs_temp.uv.v < 0)
		{
			mat = intrs_temp.mat;
		}
		else
		{
			mat = material(tm.col*tm.diffuseness*(*this->mat.tex)[intrs_temp.uv], tm.transparency, tm.reflectivity, tm.refraction_index, tm.emissivity, tm.diffuseness, tm.scattering, tm.smoothness, this->mat.tex);
		}
		return intersection(intrs_temp.point, intrs_temp.normal, mat, intrs_temp.valid, intrs_temp.inside, intrs_temp.uv);
	}

	vector<vector3> mesh::getAllPoints() const
	{
		vector<vector3> points;
		for (auto &t : triangles)
		{
			points.push_back(t.getA().coords);
			points.push_back(t.getB().coords);
			points.push_back(t.getC().coords);
		}
		return points;
	}
}
