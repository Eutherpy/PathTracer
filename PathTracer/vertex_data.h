#pragma once
#include "material.h"
#include "uv_coords.h"

namespace raycasting
{
	struct vertex_data
	{
		vector3 normal;
		uv_coords uv;
		vertex_data() = default;
		vertex_data(const vector3&, const uv_coords& = uv_coords());
	};
}
