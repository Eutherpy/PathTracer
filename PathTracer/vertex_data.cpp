#include "vertex_data.h"

namespace raycasting
{
	raycasting::vertex_data::vertex_data(const vector3 &normal, const uv_coords &uv) : normal(normal), uv(uv) { }
}
