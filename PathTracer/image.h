#pragma once
#include <vector>
#include "color.h"
#include "pixel.h"

namespace raycasting
{
	struct image
	{
		int width, height;
		std::vector<std::vector<color>> img;
		image() = default;
		image(int, int);
		void write(const char* filename, int) const;
	};
}
