#include "shape.h"

namespace raycasting
{
	raycasting::shape::intersection::intersection(const vector3 &point, const vector3 &normal, const  material &mat, bool valid, bool inside, const uv_coords &uv) : point(point), mat(mat), valid(valid), inside(inside)
	{
		this->normal = normal;
		this->uv = uv;
	};
}