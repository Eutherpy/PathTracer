#pragma once
#include <cmath>
#include <random>
#include <ostream>

namespace raycasting
{
	class vector3
	{
	protected:
		double x, y, z;
	public:
		static const vector3 X_AXIS, Y_AXIS, Z_AXIS;
		vector3(double = 0, double = 0, double = 0);
		double getX() const;
		double getY() const;
		double getZ() const;
		double magnitude() const;
		vector3 normalize() const;
		double distance(const vector3&) const;
		double sqrDistance(const vector3&) const;
		double angle(const vector3&) const;
		vector3 operator-() const;
		vector3 operator+(const vector3&) const;
		vector3 operator-(const vector3&) const;
		vector3& operator+=(const vector3&);
		vector3& operator-=(const vector3&);
		vector3 operator*(double) const;
		vector3& operator *=(double);
		friend vector3 operator*(double, const vector3&);
		double operator*(const vector3&) const;
		vector3 operator%(const vector3&) const;
		vector3 rotate(const vector3&, double) const;
		vector3 reflect(const vector3&) const;
		friend std::ostream& operator<<(std::ostream&, const vector3&);
	};
}
