#include "sphere.h"

const double DELTA = 0.000001;

namespace raycasting
{
	sphere::sphere(const vector3 &center, double r, const material &mat) : center(center), radius(r)
	{
		interpolate_normals = false;
		this->mat = mat;
	}

	shape::intersection sphere::intersect(const ray &ray, bool interpolate_normals = false) const
	{
		vector3 r_d = ray.direction;
		vector3 r_o = ray.origin;
		double b = (2 * r_d*(center - r_o));
		double temp1 = 2 * r_d*r_d;
		double discriminant = b * b - 2 * temp1*((center - r_o)*(center - r_o) - radius * radius);
		if (discriminant <= 0)
			return intersection(); //no intersection
		else if (discriminant > 0)
		{
			double temp2 = sqrt(discriminant);
			double t1 = (b + temp2) / temp1;
			double t2 = (b - temp2) / temp1;
			vector3 point, normal;
			bool inside;
			if (t2 > DELTA)
			{
				inside = false;
				point = r_o + r_d * t2;
				normal = (point - center).normalize();
			}
			else if (t1 > DELTA)
			{
				inside = true;
				point = r_o + r_d * t1;
				normal = (center - point).normalize();
			}
			else
				return intersection();
			return intersection(point, normal, mat, true, inside);
		}
		return intersection();
	}
}
