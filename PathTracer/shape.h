#pragma once
#include "ray.h"
#include "material.h"
#include "vertex_data.h"

namespace raycasting
{
	class shape
	{
	public:
		bool interpolate_normals;
		material mat;
		struct intersection : public vertex_data
		{
			vector3 point;
			material mat;
			bool valid = false, inside;
			intersection() = default;
			intersection(const vector3&, const vector3&, const material&, bool, bool, const uv_coords& = uv_coords());
		};
		virtual intersection intersect(const ray&, bool) const = 0;
	};
}
