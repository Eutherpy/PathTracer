#pragma once
#include "vector3.h"
#include "vertex_data.h"

namespace raycasting
{
	struct vertex
	{
		vector3 coords;
		vertex_data data;
		vertex() = default;
		vertex(const vector3&, const vertex_data&);
	};
}
