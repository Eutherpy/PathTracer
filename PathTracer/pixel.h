#pragma once
#include <cmath>
#include "color.h"

namespace raycasting
{
	struct pixel
	{
		unsigned char R, G, B;
		pixel() = default;
		pixel(unsigned char, unsigned char, unsigned char);
		pixel(const color&, int);
	    unsigned char clamp(double) const;
		double adjust(double, int) const;
	};
}
