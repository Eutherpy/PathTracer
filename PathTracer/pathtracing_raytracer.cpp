#include "pathtracing_raytracer.h"
#include "util.h"

using std::initializer_list;
using std::get;
using std::vector;

namespace raycasting
{
	pathtracing_raytracer::pathtracing_raytracer(const camera &cam, const initializer_list<const shape*> &shapes, const material &curr_mat,
		std::shared_ptr<realtime_drawing_manager> drm, const vector<point_light*>& lts, double sd) : renderer(cam, shapes, curr_mat, drm)
	{
		raytracer::lights = lts;
		raytracer::shadow_darkness = sd;
	}

	color raycasting::pathtracing_raytracer::radiance(int current_depth, int depth, const ray &curr_ray, const material &surrounding_mat, int diffuse_depth) const
	{
		return 0.5*pathtracer::radiance(current_depth, depth, curr_ray, surrounding_mat, diffuse_depth)
			+ 0.5*raytracer::radiance(current_depth, depth, curr_ray, surrounding_mat, diffuse_depth);
	}
}
