#pragma once
#include <vector>
#include <memory>
#include <thread>
#include "vector3.h"
#include "camera.h"
#include "shape.h"
#include "random.h"
#include "realtime_drawing_manager.h"
#include "directx_manager.h"
#include "renderer.h"

namespace raycasting
{
	class pathtracer : public virtual renderer
	{
	public:
		pathtracer() = default;
		pathtracer(const camera&, const std::initializer_list<const shape*>&, const material&, std::shared_ptr<realtime_drawing_manager>);
		virtual color radiance(int, int, const ray&, const material&, int) const override;
	};
}
