#include "color.h"

namespace raycasting
{
	color::color(double r, double g, double b) : R(r), G(g), B(b) { }

	color color::operator+(const color &col) const
	{
		return color(R + col.R, G + col.G, B + col.B);
	}

	color color::operator-(const color &col) const
	{
		return color(R - col.R, G - col.G, B - col.B);
	}

	color& color::operator+=(const color &col)
	{
		return *this = *this + col;
	}

	color& color::operator-=(const color &col)
	{
		return *this = *this - col;
	}

	color color::operator*(double k) const
	{
		return color(R*k, G*k, B*k);
	}

	color color::operator*(const color &col) const
	{
		return color(R * col.R, G * col.G, B * col.B);
	}

	color& color::operator*=(double d)
	{
		return *this = *this * d;
	}

	color& color::operator*=(const color &col)
	{
		return *this = *this * col;
	}

	color operator*(double k, const color &col)
	{
		return col*k;
	}

}