#include "ray.h"

namespace raycasting
{
	ray::ray(const vector3 &orig, const vector3 &dir) : origin(orig), direction(dir) { }
}